```mermaid
graph TD
	A[.] -->|master| B[./CONCEPTOS.md ./README.md]
	B --> |master| C[CONCEPTOS.md README.md]
  G[.] --> |tema-1| F
  F[./tema-1] --> |tema-1| D[./tema-1/tema-1.md]
  C --> |master| E[./CONCEPTOS.md ./README.md tema-1/tema-1.md]
  D --> |merge to master| E
  H[.] -->|esquema| I[./esquema.md]
  I -->|merge to master| J
  E --> |master| J[./CONCEPTOS.md ./README.md ./tema-1/tema-1.md ./esquema.md]
```