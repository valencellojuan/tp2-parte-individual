# Trabajo Práctico N°2: 
# Markdown y herramientas de programación colaborativas.

### Universidad Nacional del Comahue
### Centro Regional Universitario Bariloche

### Autor:

- Valenzuela, Juan Manuel.

### Profesores: 

- Cejas Bolecek, René
- Vilugrón, Martín.

### Catedra:

- Métodos Computacionales en Ingeniería 1 (Ingeniería Mecánica)

## Objetivos: 

Familiarizarse con el lenguaje de programación de enriquecimiento de texto Markdown.
Aprender a usar Git para control de versiones y programación colaborativa.
Aprender buenas prácticas de busqueda y clasificación de información objetiva y plasmar el contenido procesado en un informe académico.

# Estructura del informe:

- Respuestas a las consignas sobre Git y Markdown en el archivo './conceptos.md'
- Respuestas a las preguntas sobre manejo de ramas en './tema-1/tema-1.md'
- Proyecto individual sobre ordenadores simples en './procesadores/resumen-procesadores.md'
- Diagrama con el esquema de ramas utilizadas en './esquema.md'
