## Parte 1. Git y Markdown

1. Parte 1: Git
	1. Es un tipo de software que se encarga de organizar, mantener y administrar las versiones de un proyecto conformado por un conjunto de archivos. 
	2. Son las carpetas de trabajo dentro de las que se encuentran los archivos que son administrados por un sistema de software de control. La definición de repositorios "Publicos" o "Privados" se refiere a la limitación de acceso que se les dé - Cualquier individuo puede acceder a repositorios publicos, mientras que los privados requieren pasos adicionales de autenticación para permitir acceso. 
	3. Los arboles de Merkle son "una estructura de datos estratificada que tiene como finalidad relacionar cada nodo con una raíz única asociada a éste". Fuente: https://agorachain.org/arboles-merkle-usos/
	4. Una copia local es la versión de un repositorio almacenada localmente, es decir, dentro de un ordenador de trabajo. Una copia remota es la versión de un repositorio almacenada en un servidor de internet.
	5. Un commit es uno de los "estados" en que es guardado el proyecto que se está administrando.
	6. Ubicaría la versión deseada dentro del software de versiones, y utilizando el software 'actualizaría' el archivo actual a la versión deseada.
	7. Las ramas son versiones paralelas del proyecto. Se utilizan generalmente para mantener separados desarrollos de distintas partes del proyecto, que más adelante pueden ser combinadas a la rama principal del proyecto una vez estén desarrollados correctamente.

2. Markdown
	1. Markdown es un lenguaje de marcado, que permite el uso de caracteres de forma especial para darle formato a un texto de forma sencilla. 
	2. Se utiliza para dar formato a un documento, de forma que sea interpretado por un 'software Markdown' que muestre el documento en el formato deseado. La ventaja de este lenguaje es que toda la información necesaria para 'renderizar' el documento está incluida dentro del mismo. Adicionalmente, la sintaxis es sencilla de comprender y no interrumpe de manera significativa la lectura del texto, lo que hace que sea más fácil de leer y editar en caso de ser necesario, y minimiza los errores de sintaxis asociados al proceso de edición.

### Bibliografía:
	-https://medium.com/@janpoloy/qu%C3%A9-es-y-para-que-sirve-git-3fd106e6e137
	-https://rogerdudler.github.io/git-guide/index.es.html
	-https://pandao.github.io/editor.md/index.html
	-https://www.genbeta.com/desarrollo/manejo-de-ramas-de-desarrollo-con-git
