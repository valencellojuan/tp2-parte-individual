## Trabajo Práctico N°2. 
## Ejercicio 4: Informe sobre procesadores.

### Autor:
- Valenzuela, Juan Manuel - Ingeniería Mecánica

### Profesores:

- Cejas Bolecek, René
- Vilugrón, Martín

## Objetivo:
- Describir la organización de los componentes de un ordenador simple basado en el modelo de Von Neumann

## Componentes de una computadora:

Una computadora digital se compone de varios elementos:
-Un conjunto de procesadores interconectados.
-Memorias.
-Dispositivos de entrada y salida.

## Central Processing Unit:

La **Unidad Central de Procesamiento** (**CPU** por sus siglas en inglés) es el
procesador de la computadora, y es análogo al 'cerebro' en un individuo. Su
proposito general es ejecutar programas almacenados en la memoria principal,
identificando las instrucciones que los componen y ejecutandolas. Se conecta 
con los otros elementos por medio de *buses* internos, cables paralelos que 
transmiten datos y señales entre los componentes.

El CPU, a su vez, está dividido en tres partes principales:
1. Unidad de control: Busca instrucciones en la memoria principal y determina 
el tipo de operación a realizar.
2. Unidad de aritmética y lógica(ALU): Realiza operaciones aritméticas (ej: Suma) 
y operaciones lógicas (ej:AND, OR, XOR, etc.) solicitadas por las 
instrucciones.
3. Memoria cache: Memoria interna de alta velocidad, 
dividida en "Registros", fragmentos de memoria en los que se almacenan
datos temporales e información de control con lectura y escritura rápida (ya 
que están dentro del CPU). Entre los más importantes se encuentran:
  -Registro de contador de programa (PC): Permite almacenar la siguiente 
instrucción a ser ejecutada.
  -Registro de instrucciones (IR): Contiene la instrucción que se está 
ejecutando en el momento.

## Memoria

La memoria es el componente físico de la computadora en que se almacenan 
datos y programas. Es aquí a donde accede el CPU para leer o escribir 
información, y el sistema sería obsoleto sin este componente.

Las memorias se dividen en dos categorías:
1. Memoria primaria: Este tipo de memoria se utiliza para almacenar el 
programa en ejecución. El procesador puede comunicarse con un 'tiempo de 
acceso' muy pequeño a esta memoria (aproximadamente nanosegundos) y esta 
comunicación es independiente de la dirección de memoria a la que se está 
accediendo.
2. Memoria secundaria: Este tipo de memoria tienen tiempos de acceso mucho
más largos (aproximadamente milisegundos) y esta velocidad de comunicación 
depende de la ubicación de los datos a los que se está accediendo con el 
procesador.

Físicamente, las memorias están compuestas por un conjunto de celdas que 
poseen una "dirección", un número con el cual el procesador puede acceder a 
ellas y a la información que almacenan. Cada celda puede almacenar una cierta
cantidad de '**bits**', siendo el bit la unidad más simple de información (un
valor binario que solo puede ser '0' ó '1').

## Dispositivos de Entrada y Salida:

Los dispositivos de entrada y salida son elementos del ordenador que 
permiten a un usuario interactuar con el ordenador. Sin estos elementos sería
 imposible utilizarlo. En terminos simplificados, los dispositivos de 
entrada permiten ingresar información al ordenador, mientras que los de 
salida permiten interpretar o utilizar información procesada por el ordenador
 de una o multiples formas.

Los componentes de entrada y salida constan de dos partes:
-Un controlador(Driver): Estos son los circuítos electrónicos que permiten la 
comunicación entre el dispositivo y el ordenador.
-El periferico: Este es el elemento 'fisico' del dispositivo con el que el 
usuario interactua (ej: Teclado, Mouse, Monitor, etc.)

## Fuente bibligráfica:
"Organización de Computadoras. Un enfoque estructurado.", 4° edición.
Autor: Andrew S. Tanenbaum
Páginas 39-41, 56-58, 89-91 , 113.
-http://f.javier.io/rep/books/Tanenbaum,%20Andrew%20S.-%20Organizaci%C3%B3n%20de%20Computadoras.%20Un%20Enfoque%20Estructurado.%20Cuarta%20Edici%C3%B3n.%20M%C3%A9xico,%20Prentice%20Hall,%202000.pdf
